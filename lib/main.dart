import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black
          )
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500
        )
      ),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget()
        ),

    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDiretionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_outlined,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget mobilePhoneListTile(){
  return ListTile(
  leading: Icon(Icons.call),
    title: Text('081-5553625'),
    subtitle: Text('mobile'),
    trailing:IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: (){},
    ),
);

}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text('155-570-0000'),
    subtitle: Text('other'),
    trailing:IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: (){},
    ),
  );

}
Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text('63160226@go.buu.ac.th'),
    subtitle: Text('work'),
    trailing: Text("")

  );

}
Widget addressListTile(){
  return ListTile(
      leading: Icon(Icons.location_on),
      title: Text('Bang saen'),
      subtitle: Text('home'),
      trailing: IconButton(
        icon: Icon(Icons.assistant_direction),
        color: Colors.teal,
        onPressed: (){},
      ),

  );

}
AppBar buildAppBarWidget() {
  return AppBar(
  backgroundColor: Colors.teal,

  leading: Icon(
  Icons.arrow_back,
  color: Colors.white,
  ),
  actions: <Widget>[
  IconButton(
  onPressed: (){},
  icon: Icon(Icons.star_border),
  color: Colors.white
  )
  ],
  );
}
Widget buildBodyWidget(){
  return ListView(

    children: <Widget>[
      Column(
        children:  <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child:

            Image.network(
              "https://github.com/ptyagicodecamp/educative_flutter/raw/profile_1/assets/profile.jpg?raw=true",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(" Pattraporn Noothong",
                    style : TextStyle(fontSize: 30),
                  ),
                ),

              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8,bottom: 😎,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly
              ,
              children: <Widget>[
                //call action Item

                buildCallButton(),
                buildTextButton(),
                buildVideoCallButton(),
                buildEmailButton(),
                buildDiretionsButton(),
                buildPayButton(),

              ],

            ),

          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color:  Colors.grey,

          ),
          emailListTile(),
          addressListTile()
        ],
      )
    ],
  );
}




